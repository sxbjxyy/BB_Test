﻿using FreeSql;

namespace RR.Entity;

public class RoleEntity:BaseEntity<RoleEntity,int> 
{
    public string RoleName { get; set; }
    
}