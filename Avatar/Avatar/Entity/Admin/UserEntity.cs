﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using FreeSql;
using FreeSql.DataAnnotations;

namespace RR.Entity;

public class UserEntity:BaseEntity<UserEntity,int>    //int 表示自动创建ID
{

    [Description("用户名")]
    public string? LoginName { get; set; }

    [DisplayName("真实姓名")]               
    public string RealName { get; set; }
    
    public int RoleId { get; set; }

    [DisplayName("头像")]
    public string? AvatarPath { get; set; }


    [Navigate(nameof(RoleId))]
    public RoleEntity Role { get; set; }

   
}
